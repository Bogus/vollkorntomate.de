---
title: "Über mich"
date: 2022-10-11
draft: false
description: 'Hallo! Ich bin Thomas. – vollkorntomate'
---

---

## Hallo! Ich bin Thomas. 👋

Ich bin 22 Jahre alt und wohne in München und Darmstadt. Ich habe einen Bachelor in Informatik von der Technischen Universität München und mache aktuell einen Master in IT-Sicherheit an der TU Darmstadt. Über die Jahre habe ich einige Erfahrung in der Softwareentwicklung gesammelt, vor allem in Java, Swift und C#.

Ich bin ein Fan von Open-Source-Software, offenen Daten und der Community dahinter. Ich bin ein Verfechter von Datenschutz und digitalen Rechten für ein freies und offenes Internet. [Öffentliche Daten nützen, private Daten schützen.](https://www.ccc.de/de/hackerethics)

Darüber hinaus lerne ich gerne Sprachen. Deutsch ist mein Muttersprache, Englisch kann ich fließend. Ich kann Französisch und Norwegisch konversationssicher und verstehe zumindest Grundlagen in Spanisch, Niederländisch, Arabisch und Ukrainisch.