---
title: "About Me"
date: 2022-10-11
draft: false
description: 'Hi! I am Thomas. – vollkorntomate'
---

---

## Hi! I am Thomas. 👋

I am 22 years old and live in Munich and Darmstadt, Germany. I have a bachelor's degree in informatics from the Technical University of Munich. Currently, I am doing a master's degree in IT security at the Technical University of Darmstadt. Throughout the years, I came to gain some experience in software development, mostly in Java, Swift and C#.

I love open-source software, open data and the community behind it. I am a defender of privacy and digital rights for an open internet. [Make public data available, protect private data.](https://www.ccc.de/en/hackerethics)

Besides technology, I like learning languages. I speak German natively, English fluently and I consider myself conversational in French and Norwegian. I can understand and speak at least basics in Spanish, Dutch, Arabic and Ukrainian.