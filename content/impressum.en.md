---
title: "Imprint"
date: 2022-07-09T00:41:20+02:00
draft: false
description: "Imprint"
---

---

According to German laws, I am obligated to publish personal data about me here. More specifically, § 5 TMG and § 55 Sect. 2 RStV force me to state my full name as well as a postal address on this page. An anonymous mailbox is thereby not sufficient.

I see this as an intrusion into my personal data protection rights, since I don't want the whole world to know where I live. That's why I deny publishing this information here.

If you have a legitimate interest in obtaining this data, please do not hesitate to write me an e-mail or contact me via one of the several other ways stated on the [home page](/).

E-Mail: impressum@vollkorntomate.de\
PGP Fingerprint: `8003 4009 F358 2127 17F1 9AB3 8738 4A83 6B85 D103`[^pgpdownload]

In general, I favor the obligation to have an imprint for business-like websites, meaning a natural or legal person earning money with or through this website. However, the definition for "business-like" is very fluent in German law, and the imprint obligation already applies when contents are "usually delivered against payment". I am not earning any money with this website – and I don't have any intention in doing so.

[^pgpdownload]: Download public key from [this server](/files/pubkey.asc) or [keys.opengpg.org](https://keys.openpgp.org/vks/v1/by-fingerprint/80034009F358212717F19AB387384A836B85D103)