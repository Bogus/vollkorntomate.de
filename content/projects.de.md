---
title: "Projekte"
date: 2022-10-10
draft: false
description: "Übersicht über Projekte, an denen ich arbeite bzw. gearbeitet habe"
---

---

- **photon-geocoding** ([Codeberg](https://codeberg.org/vollkorntomate/photon-geocoding-rs) / [GitHub](https://github.com/vollkorntomate/photon-geocoding-rs) • [crates.io](https://crates.io/crates/photon-geocoding)) \
Ein API client für Komoot's Photon API, geschrieben in und für Rust. \
{{< secondary >}}Rust{{< /secondary >}}

- **flutter_photon** ([GitHub](https://github.com/vollkorntomate/flutter-photon/) • [pub.dev](https://pub.dev/packages/flutter_photon)) \
Ein API client für Komoot's Photon API, geschrieben in und für Dart/Flutter. \
{{< secondary >}}Dart{{< /secondary >}}

---

# Beruflich & Ausbildung

- **Juli 2022 – September 2022** \
Praktikum als Penetration Tester \
{{< secondary >}}Python | C | Automotive | CAN | Code Audit{{< /secondary >}}

- **November 2021 – April 2022** \
Bachelorarbeit in Zusammenarbeit mit dem [Fraunhofer AISEC](https://www.aisec.fraunhofer.de/) \
Titel: Grey-Box Fuzzing eingebetteter Systeme unter Zuhilfenahme von Seitenkanalinformationen \
{{< secondary >}}Python | C | Embedded | AFL++ | TensorFlow/Keras{{< /secondary >}}

- **April 2021 – Juli 2021** \
Praxiskurs *iOS Praktikum* an der TUM \
{{< secondary >}}Swift | iOS | SwiftUI | Combine | GraphQL{{< /secondary >}}

- **April 2021** \
Science Hack (Hackathon) von TUM Junge Akademie \
{{< secondary >}}Python | Django{{< /secondary >}}

- **18 Monate in 2019 bis 2021** \
Praktikant und Werkstudent bei einer Softwareentwicklungsfirma \
{{< secondary >}}C# | Microsoft Azure{{< /secondary >}}

---

Weitere (kleine) Beteiligungen sind auf meinem [GitHub-Profil](https://github.com/vollkorntomate) zu finden.
