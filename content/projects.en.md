---
title: "Projects"
date: 2022-10-10
draft: false
description: "An overview of the projects I am/was working on"
---

---

- **photon-geocoding** ([Codeberg](https://codeberg.org/vollkorntomate/photon-geocoding-rs) / [GitHub](https://github.com/vollkorntomate/photon-geocoding-rs) • [crates.io](https://crates.io/crates/photon-geocoding)) \
An API client for Komoot's Photon API written in and for Rust. \
{{< secondary >}}Rust{{< /secondary >}}

- **flutter_photon** ([GitHub](https://github.com/vollkorntomate/flutter-photon/) • [pub.dev](https://pub.dev/packages/flutter_photon)) \
An API client for Komoot's Photon API written in and for Dart/Flutter. \
{{< secondary >}}Dart{{< /secondary >}}

---

# Professional & Education

- **July 2022 – September 2022** \
Internship as a Penetration Tester \
{{< secondary >}}Python | C | Automotive | CAN | Code Audit{{< /secondary >}}

- **November 2021 – April 2022** \
Bachelor's Thesis in cooperation with the [Fraunhofer AISEC](https://www.aisec.fraunhofer.de/en.html) \
Title: Grey-Box Fuzzing Embedded Devices using Feedback from Side-Channel Analysis \
{{< secondary >}}Python | C | Embedded | AFL++ | TensorFlow/Keras{{< /secondary >}}

- **April 2021 – July 2021** \
Practical cource about iOS at TUM \
{{< secondary >}}Swift | iOS | SwiftUI | Combine | GraphQL{{< /secondary >}}

- **April 2021** \
Science Hack (Hackathon) by TUM Junge Akademie \
{{< secondary >}}Python | Django{{< /secondary >}}

- **18 Months in 2019 to 2021** \
Intern and working student at a software engineering company \
{{< secondary >}}C# | Microsoft Azure{{< /secondary >}}

---

Further (minor) contributions can be found on my [GitHub profile](https://github.com/vollkorntomate).